class ScoreCard
{
private:
    std::vector<int> scores; // Scores for each category
public:
    ScoreCard(int numCategories) : scores(numCategories, -1) {}

    void setScore(int category, int score)
    {
        scores[category] = score; // Set the score for a category
    }

    int getScore(int category) const
    {
        return scores[category]; // Get the score for a category
    }

    bool isCategoryFilled(int category) const
    {
        return scores[category] != -1; // Check if a category is filled
    }

    int getTotalScore() const
    {
        int total = 0;
        for (int score : scores)
        {
            if (score != -1)
                total += score;
        }
        return total;
    }
};