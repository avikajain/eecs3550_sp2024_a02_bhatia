# EECS3550_SP2024_A04_BHATIA



## Getting started

Welcome to our modified version of the Yahtzee game. This project is the demo product built by a three-memebers teams including: Avika Jain, Jayant Bhatia, and Tran Bao Ngoc Nguyen.

This README.md file will help you understand the main function of this modified Yathzee game, and it also includes our contact email for answering any of your questions.  

## Welcome to our modified version of the Yahtzee game project

## Description
This project is a version control-enabled modification of the Yahtzee game. Yahtzee is a traditional dice game in which players roll five dice to create specific combinations and score points based on those combinations. Check out https://en.wikipedia.org/wiki/Yahtzee to learn more about the game and its scoring rules. Players can score in a variety of categories, including "Full House," "Small Straight," and "Yahtzee" (five of a kind). We're building a single-player version. Unlike traditional Yahtzee, where a user can keep individual dice and re-roll, in this game, a user can only accept one score each roll. So, typical gameplay would be as follows:
1. Roll the dices,
2. Select a score/category,
3. Repeat step 1 and 2 until all categories are filled,
4. Automatically calculate and display the final score.

The following guidelines are applied: 
Implementation The game must be programmed in C++. 
Class Structure: Your project should contain at least four classes: 
1. Die: This represents a single dice. 
2. Dice: Controls a series of dice. 
3. ScoreCard: Tracks the player's scores in many categories. 
4. Game: Manages the game flow and interactions. 

The main method in main.cpp will then create a Game object and use Game.play() to start the game.

## Installation
### Prerequisites
- C++ compiler (e.g., g++)
- CMake (optional, for building)

### Building the Game
1. Clone the repository:

    ```bash
    git clone <repository_url>
    ```
Make a folder with the name of Yahtzee-Game and save the repository in that folder.

2. Navigate to the project directory:

    ```bash
    cd Yahtzee-Game
    ```

3. Compile the code using your preferred method. For example, using g++:

    ```bash
    g++ -o yahtzee main.cpp
    ```

    or using CMake:

    ```bash
    cmake .
    make
    ```

## Usage
1. After building the game, execute the binary file:

    ```bash
    ./yahtzee
    ```
## Output: 
1- Output after rolling 5 dice and display of categories to choose from

<img width="440" alt="first" src="https://github.com/jayantbhatia/Hospitality_domain_analysis/assets/90919276/d25678bc-36dc-4a2c-b3f6-9c875b333b29">

2-Updated ScoreCard after choosing the category and playing the next round

<img width="440" alt="Second" src="https://github.com/jayantbhatia/Hospitality_domain_analysis/assets/90919276/70b95100-9633-4b2f-ac60-0519e0f13c91">

3-The final Updated scorecard with all categories filled along with the total final score

<img width="440" alt="third" src="https://github.com/jayantbhatia/Hospitality_domain_analysis/assets/90919276/ef5e2fb2-4902-4431-bb07-0daf7870b1fa">

Above was my example game for your reference. 
Feel free to enjoy the game yourselves and let us know your thoughts/ opinions.

## Evidence of resolving the merge-conflict 

<img width="1508" alt="Screenshot 2024-04-08 at 9 49 38 PM" src="https://github.com/Avikaj/Image-File-Encryption-Decryption/assets/151662350/3062080f-d745-476b-ac70-be5a78f999af">


By performing three way merges

<img width="1512" alt="Screenshot 2024-04-08 at 9 50 49 PM" src="https://github.com/Avikaj/Image-File-Encryption-Decryption/assets/151662350/e2d6cd0e-06ae-4a5a-8c6a-f4c05bc7c6b0">


## Contributing
We are looking forward to receiving your contributions. While requesting for merging your feature branches, please uncheck the option which mentions deleting the source code. By doing this, we don't loose our source code while merging your branch. This is for the ability to recover the main program for this project if some big issues happen.      

## Authors and acknowledgment
Thank you for your time reading this README.md file and playing this modified version of the Yathzee game. We wholeheartedly appreciate all of your contribution to our project. 

Looking forward to receiving your supports and contributions. 

## License
This project is licensed under Jayant Bhatia, Avika Jain, and Tran Bao Ngoc Nguyen. The ideas for this project is the Yahtzee game. Please refer to this following link to enjoy the full Yahtzee game yoursleves: https://en.wikipedia.org/wiki/Yahtzee
