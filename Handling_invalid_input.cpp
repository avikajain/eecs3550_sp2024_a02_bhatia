
            bool validInput = false;

            do
            {
                std::cout << "Choose a category to score:\n";
                std::cout << "___________\n";
                std::cout << " 0 - Ones\n"
                          << "           \n";
                std::cout << " 1 - Twos\n"
                          << "          \n";
                std::cout << " 2 - Threes\n"
                          << "        \n";
                std::cout << " 3 - Fours\n"
                          << "         \n";
                std::cout << " 4 - Fives\n"
                          << "         \n";
                std::cout << " 5 - Sixes\n"
                          << "         \n";
                std::cout << " 6 - Three of a Kind\n"
                          << "\n";
                std::cout << " 7 - Four of a Kind\n"
                          << " \n";
                std::cout << " 8 - Full House\n"
                          << "    \n";
                std::cout << " 9 - Small Straight\n"
                          << "\n";
                std::cout << " 10 - Large Straight\n"
                          << "\n";
                std::cout << " 11 - Yahtzee\n"
                          << "      \n";
                std::cout << " 12 - Chance\n"
                          << endl;
                std::cout << "__________\n";
                std::cout << "Your chosen category is: ";

                if (!(std::cin >> chosenCategory))
                {
                    std::cerr << "Invalid input. Exiting the program." << std::endl;
                    exit(EXIT_FAILURE);
                }

                if (chosenCategory < 0 || chosenCategory > 12)
                {
                    std::cerr << "Invalid category. Please choose a category between 0 and 12." << std::endl;
                }
                else if (scorecard.getScore(chosenCategory) != -1)
                {
                    std::cerr << "Category already filled. Please choose another category." << std::endl;
                }
                else
                {
                    validInput = true;
                }
            } while (!validInput);