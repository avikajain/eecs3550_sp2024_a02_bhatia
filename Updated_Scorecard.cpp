string AllCategories[13] = {"Ones", "Twos", "Threes", "Fours", "Fives", "Sixes", "Three of a Kind", "Four of a Kind", "Full House", "Small Straight", "Large Straight", "Yahtzee", "Chance"};

for (int i = 0; i < 13; ++i)
            {
                if (scorecard.getScore(i) != -1)
                {
                    std::cout << AllCategories[i] << ": " << scorecard.getScore(i) << std::endl;
                }
                else
                {
                    std::cout << AllCategories[i] << ": -" << std::endl;
                }
            }

            std::cout << "---------------------\n";