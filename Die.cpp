class Die
{
private:
  int value = 1;	// Current value of the die
public:
    Die ()
  {
	roll ();	// Roll the die when it's created
  }
  void roll ()
  {
	value = rand () % 6 + 1;	// Generate a random value between 1 and 6
  }
  int getValue () const
  {
	return value;
  }
};
