class Dice
{
private:
    std::vector<Die> dice;

public:
    Dice(int numDice)
    {
        for (int i = 0; i < numDice; ++i)
        {
            dice.push_back(Die());
        }
    }

    void rollAll()
    {
        for (auto &die : dice)
        {
            die.roll();
        }
    }

    int getValue(int index) const
    {
        return dice[index].getValue();
    }
};