        int score = calculateScore (chosenCategory);	// Calculate score for the chosen category
		scorecard.setScore (chosenCategory, score);	// Set the score for the chosen category
		

        int calculateScore (int category)
  {
	int score = 0;
	bool three = false;
	bool two = false;
	bool straight = true;
	switch (category)
	  {
	  case 0:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 1)
			  {
				score += 1;
			  }
		  }
		break;
	  case 1:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 2)
			  {
				score += 2;
			  }
		  }
		break;
	  case 2:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 3)
			  {
				score += 3;
			  }
		  }
		break;
	  case 3:					// Fours
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 4)
			  {
				score += 4;
			  }
		  }
		break;
	  case 4:					// Fives
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 5)
			  {
				score += 5;
			  }
		  }
		break;
	  case 5:					// Sixes
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 6)
			  {
				score += 6;
			  }
		  }
		break;
	  case 6:
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count >= 3)
			  {
				// Calculate the sum of all dice
				for (int i = 0; i < 5; ++i)
				  {
					score += dice.getValue (i);
				  }
				break;
			  }
		  }
		break;
	  case 7:					// Four of a Kind
		// Check if there are at least four dice with the same value
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count >= 4)
			  {
				// Calculate the sum of all dice
				for (int i = 0; i < 5; ++i)
				  {
					score += dice.getValue (i);
				  }
				break;
			  }
		  }
		break;
	  case 8:					// Full House
		// Check if there is a three of a kind and a pair
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count == 3)
			  {
				three = true;
			  }
			if (count == 2)
			  {
				two = true;
			  }
		  }
		if (three && two)
		  {
			score = 25;
		  }
		break;
	  case 9:
		for (int i = 1; i <= 3; ++i)
		  {
			bool straight = true;
			for (int j = i; j < i + 4; ++j)
			  {
				bool found = false;
				for (int k = 0; k < 5; ++k)
				  {
					if (dice.getValue (k) == j)
					  {
						found = true;
						break;
					  }
				  }
				if (!found)
				  {
					straight = false;
					break;
				  }
			  }
			if (straight)
			  {
				score = 30;
				break;
			  }
		  }
		break;
	  case 10:
		for (int i = 1; i <= 5; ++i)
		  {
			bool found = false;
			for (int j = 0; j < 5; ++j)
			  {
				if (dice.getValue (j) == i)
				  {
					found = true;
					break;
				  }
			  }
			if (!found)
			  {
				straight = false;
				break;
			  }
		  }
		if (straight)
		  {
			score = 40;
		  }
		break;
	  case 11:					// Yahtzee
		// Check if all dice have the same value
		for (int i = 1; i <= 6; ++i)
		  {
			int count = 0;
			for (int j = 0; j < 5; ++j)
			  {
				if (dice.getValue (j) == i)
				  {
					count++;
				  }
			  }
			if (count == 5)
			  {
				score = 50;
				break;
			  }
		  }
		break;
	  case 12:					// Chance
		// Sum of all dice
		for (int i = 0; i < 5; ++i)
		  {
			score += dice.getValue (i);
		  }
		break;
	  }
	return score;
  }
